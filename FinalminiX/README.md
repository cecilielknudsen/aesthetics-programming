![](finalminiX.png)

**The Ideal World** 

_Group 11: Mie Buch Enemark, Frederikke Nissen Breum, Frederikke Skjoldborg Simonsen & Cecilie Langkjær Knudsen_

In order to see the code and program, I'll direct to this URL:

[The program](https://gitlab.com/frederikke.n.breum/aesthetic-programming/-/tree/master/finalminix)

[The project](https://frederikke.n.breum.gitlab.io/aesthetic-programming/finalminix)
