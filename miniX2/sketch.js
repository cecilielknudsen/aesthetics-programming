let s= "Press mouse to wake up"
let z= "z"
let Z="Z"
function setup() {
  createCanvas(600, 400);
}

function draw() {
  clear();
  background(205,7,50);

  //Textbox
  fill(0)
  strokeWeight(7)
  textSize(20);
  text(s,140,300)

  //making the ellipse to wake it up
  if(mouseIsPressed){
    fill(250,204,0);
    ellipse(120,150,200,200);
    fill(255);
    ellipse(150,120,30,30);
    ellipse(90,120,30,30);
    fill(0);
    ellipse(90,120,10,10);
    ellipse(150,120,10,10);
    arc(120,175,40,75,100,PI + QUARTER_PI);
  } else{
    //the second smiley
    fill(250,204,0);
    ellipse(420,150,200,200);
    strokeWeight(4.5)
    line(460,110,435,110);
    line(375,110,400,110);
    line(400,190,440,190);

    textAlign(LEFT);
    textSize(35);
    fill(0)
    text('z',501,80);
    text('Z',512,60);
    text('z',530,30);
  }
}
