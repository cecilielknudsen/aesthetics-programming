[See my project here!](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX2/)

![Screenshot of my project](class_02.png)

**Describe your program and what you have used and learnt.**

For my second coding project, we were tasked with making two emojis and I wanted to keep in mind the assigned reading while making it. I brainstormed for something as universal as possible, which is why I decided to go with the original yelow smiley, as it is just familiar enough for the viewer to relate to it, but not human-like enough for people to feel misrepresented. For instance, the newer Apple emojis can change features (color of skin, hair, gender etc.), which can feel alienating for some. 

**The program:**

I started by setting the variables, before the setup. 
![Screenshot](class_02_variables.png)

These variables are places here, above the function setup, in order for the text to be able to show. When wanting to summon them, one only has to write: e.g.: text(s,x,y)

![Screenshot](class_02_functionsetup.png)

This is only the size of the canvas.

![Screenshot](class_02_functiondraw.png)

First in the function draw, I wanted the emojis to disapear when the mouse was pressed, so above all, it says clear(), which means that it will clear the desired background (in this case one of the emojis), when the mouse is pressed down.

The next segment is the textbox, which I wanted to be almost a constant. The syntax used here among other: fill() and strokeWeight(). I also used textSize() to give the text the desired size. 

Last but not least, we have what sets it all in motion, the if/else statement. I wrote the code, so that when the mouse is pressed down, the "sleeping emoji" disapears and another emoji shows, and when releasing the mouse the sleeping emoji comes back. The last segment is the z´s of the sleeping emoji, which is written as a variable in the top segment. 

I had a lot of trouble with getting the if/else statement to work, which was very frustrating at times, and I also had to "give up" some code like setInterval() (I wanted the z's to blink separately, however I couldn't get it to work) and try considering other alternatives. I'm proud of what I've accomplished, as I don't have any coding experience. I think what I've made is cute :) 

**How would you put your emoji into a wider social and cultural context that
concerns a politics of representation, identity, race, colonialism, and so on?**

As I've mentioned previously, I wanted to make something that one could relate to and still feel represented by. As I see it, the yellow color becomes quite neutral compared to the "flesh" colored emojis that exists out there in the world. Also I wanted to make it as universal as possible, and my thought-process behind it was that everybody needs sleep, or feel sleepy etc. It's not really something that can divide people as it is a bodily function any living being needs to survive. As well as the "smiley emoji", which pops up when the mouse is pressed down - everybody wants to smile and be happy and no matter what social and cultural context one comes from, one could argue that this is also something one needs to survive (what is life without happiness). There is a reason why we still use the old-school yellow emojicons; it's because it's we'll always be able to relate to and come back to.   

