let s="please wait for love..."
function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
    frameRate(8);
}
function draw() {
  // put drawing code here
  background(255, 170, 238,80);
  noStroke();
  drawElements();
  noStroke();
  textSize(132);
  text(s,100,300);
  fill(87, 5, 70);
  text(s,100,600);
  fill(153,49,132);
  text(s,100,900);

}

function drawElements() {
let num=9;
push();
translate(width/2,height/2);
let cir=360/num*(frameCount%num);
rotate(radians(cir));
noStroke();
fill('red');
  beginShape();
	vertex(200,350);
	bezierVertex(200,250,350,200,350,150);
	bezierVertex(350,100,250,50,200,140);
	bezierVertex(150,50,50,100,50,150);
	bezierVertex(50,200,200,250,200,350);

	endShape();
pop();
stroke(255,255,0);

}

function windowResized() {
  resizeCanvas(windowWidth,windowHeight);
}
