**MiniX3 - The throbber**

[See my project here](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX3/)

![](miniX3_class03.png)

**What do you want to explore and/or express?**

In this weeks assignment, we were tasked with making a throbber and I really wanted to explore different kinds of shapes and sizes. I was very inspired by Valentine's Day, which was last week, and thus I decided to try out heartshapes. The heart I made out of bezierVertex, which is kind of like the shape of a cresent moon (half-moon) - so to make it complete, I used 4 of these.  
The syntaxes used for this work are noStroke, drawElements, frameRate, rotate, fill, push and pop among other things. 

As I mentioned, I was very inspired by love and Valentine's Day, and I thought about how one is told to wait for their true love. Therefore, I thought I would be interesting if you had to wait for your true love online - similar to dating apps.

**What are the time-related syntaxes/functions that you have used in your
program, and why have you used them in this way?**

The time-related syntaxes in my program is the rotate function, which makes the heart go in circles, i.e to make the throbber.

**Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?**

Normally, when an individuel encounter a throbber or loading icon, as many know it as, they associate it with waiting time. When you watch a video on YouTube and scroll through Instagram, the loading icon is to many people indicating that something is being loaded down to their device - e.g. because of the internet being slow or to refresh you page.
What I have now learn, it's often not like that. Sometimes it is part of the experience - (e.g. Instagram refreshing) to let the user know they have watch all the new post etc. and sometimes it's due to the fact that an intermediary is connecting one through the platforms. These two examples are view differently, because the first (YouTube) is not something you want when you are streaming, as you want the video to run as smoothly as possible, whereas refreshing Instagram can feel more as an accomplishment and is not a bad thing.

