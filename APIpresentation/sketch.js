let weather;
let api = 'http://api.openweathermap.org/data/2.5/weather?q=';
let apiKey = '&APPID=78c655cac8550b8842b39bd742a1bc3c';
let units = '&units=metric';

function setup() {
  createCanvas(640,480);

  let button = select('#submit');
  button.mousePressed(weatherAsk);

  input = select ('#city');
}

function weatherAsk(){
  let url = api + input.value() + apiKey + units;
  loadJSON(url, gotData);
}

function gotData(data){
  //println(data);
  weather = data;
}
function draw() {
  background('#fae');
  if (weather){
    var temp = weather.main.temp;
    var humidity = weather.main.humidity;
    noStroke();
    ellipse(100,100,temp,temp);
    ellipse(300,100,humidity,humidity);
    fill(255);
    textSize(17);
    text('Temperature',70,170);
    text('Humidity',280,170);

  }

}
