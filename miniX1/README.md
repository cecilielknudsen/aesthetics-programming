RUNME MiniX1: https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX1/ 
![](miniX1_class.01.png)

In this miniX1 assignment it was all about trying to get more familiar with Atom and coding with the p5.js library. 

First, I tried to look through the p5.js reference site, in order to get some inspiration of what I could make. I then decided to take some inspiration from our 'getting started' exercises - I wanted to make some ellipses in different sizes, scattered across the canvas. The most challenging about this, was figuring out where to put the ellipses on the canvas, so to speak - It is more than just "putting it there", one has to keep in mind the x and y-axes as well as obviously the width and height of the ellipse in order to not intersect each other. I used the following code for each ellipse: ellipse(x,y,w,h)

Furthermore, after getting all the placements right, I knew I wanted to do something with color, so it wasn't so black and white (and boring), therefore I decided to change the backgroundcolor as well as the outline of the ellipses.

Moreover, I knew I wanted some sort of interaction, so I then decided to add two if-else statements, to help control the flow of my code. The first if-else code is "mouseIsPressed", which means that when you press down (click) on your mouse, the ellipses changes color, and the sencond code if-else code is "mouseY < X",which makes the mouse be bigger the closer it is to the y-axes.

I found my first coding experience rewarding and at the same time challenging. Rewarding in the sense that when you write the code and it actually work is great, but at the same time when it doesn't work it can become extremely frustrating and one patience really get tested. I think a lot of the features I coded, came down to sheer common sense - logic. E.g. to place the ellipses I must take the x- and y-axes into consideration, as well as width and height must be the same in order to actually make it into an ellipse.

The coding process, I believe, is more like, as I mentioned previously, having a common sense - to think logically. Coding is similar to writing and reading in the syntax, which is the way we know and understand what specifically is being said. It is also different from reading and writing in the way that it is almost like a foreign language, which means that I feel like I have to learn a whole new language that has math involved when learning to code, which is indeed challenging and something that need a bit of time to get used to.

Coding and programming has, for me, always been about getting/making instructions for the software/hardware to follow. I know it's what makes websites, app, etc. to work and run, and I've always thought it was probably more complecated than I possibly could imagine. However, I never thought of coding and programming as a thinking process or a tool to make art with, so to discover that, is something that makes me extremely excited.    
