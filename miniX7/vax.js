function Vax(){
  //makes the icon go up and down on the same spot
  this.y = height/2;
  this.x = 64;

  this.gravity = 1;
  this.lift =  - 12;
  this.velocity = 0;

  this.show =function(){
      image(sImg, this.x, this.y, 60, 60);
  }
  this.up = function(){
    this.velocity += this.lift;
  }

  this.update = function(){
    this.velocity += this.gravity;
    //this.velocity += 0.9;
    this.y += this.velocity;

    if (this.y > canvas.height){
      this.y = height;
      this.velocity=0;
    }
    if (this.y < 0){
      this.y = 0;
      this.velocity=0;
    }
  }
}
