**CORONA CATCHER**

[Click here to see my game](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX7/)

![](miniX7_screenshot.png)

- **HOW MY GAME WORK** I took inspiration from Daniel Shiffman's Coding Challenge and a very popular game _"Flappy Bird"_ for this project and I decided that was what I wanted to do. From the start I wanted it to have some sort of relevance to what is happening in the world and thus the game became about Covid-19. The primary objective of the game is that the vaccine has to go through obstacles to get to the corona cells. The way one plays is just like _Flappy Bird_ - by using the spacebar to "jump", one moves the vaccine-icon up (if one doesn't, the icon goes down) and then one has to get the icon through the space between the pipes, without touching them. If one touches the pipes, it's GAMEOVER. However, there is only a gameover, never a _"YOU WIN"_, so technically you could be playing forever. While playing the game, there is gamemusic playing in the background. With a slider at the bottom of the canvas, one can turn up and down for the music.   

- **THE OBJECTS IN THE GAME** There are two things in the program, which can be defined as objects. To better control these objects, I created two classes - one is defined for the pipes and one defined for the vaccine (vax). It makes a lot of sense creating these classes (almost like making a sketch only for the object) for the objects, as it helps keep a good overview to the main sketch as well. For instance, when designing the pipes and their functions, I simply wrote it all into the pipes class, and then "called it forth" in the main sketch through variables.  

- **OBJECT-ORIENTED PROGRAMMING** To create objects with certain functions could one argue is one of the main characteristics of object-oriented programming (OOP). As mentioned in Winnie's book:  _"...object-oriented programming is highly organized and concrete even though objects are abstractions. it's also worth reitering that OOP is designed to reflect the way the world is organised and imagined, at least from the computer programmers' perspective."_  OOP is also the way many understand objects and their interactions. By creating objects in programming, such as a game for instance, it makes it more manageable to comprehend, because, as I mentioned previously, one has created a object in one file(sketch), but its interactions are created in another (main sketch) through varibles. 

- **THE CULTURAL CONTEXT** For this project, I decided to read all of the readme-questions first, so I knew what I wanted/needed to focus on. So as I mentioned above, I wanted my game to have some sort of relevance to what is going on in the world. And as we all know, Covid-19 has harassed us for some time now. I made the game so that there is only a GAMEOVER and not a YOU WIN function, because as I'm sure a lot of people feel like this s***show never ends. Some might argue that it's quite dark and cynical. Maybe some day, when everybody has gotten the vaccine and we all can go outside and actually see peoples smiles again, then I can add a YOU WIN function, but for now, this game represents how the current situation is and how we all might feel - it's never-ending. So as we're already sitting at home, we might as well play games while we're at it.   

Enjoy :) 

**REFERENCES**

- [Coding Train - Flappy Bird](https://www.youtube.com/watch?v=cXgA1d_E-jY&list=PLRqwX-V7Uu6aRpfixWba8ZF6tJnZy5Mfw&index=4)

- [Magic Monk - start screen](https://www.youtube.com/watch?v=TgHhEzKlLb4)

- [Soundtrack for game](https://vgmsite.com/soundtracks/pokemon-cafe-mix-2020-ios-switch/ndxkhhgfml/BGM%20Stage%205.mp3)

- [Inspiration for start/gameover screens](https://editor.p5js.org/ehersh/sketches/Hk52gNXR7)

- **BOOK:** Soon Winnie & Cox, Geoff, _"Object abstraction"_, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

- **BOOK:** Matthew Fuller and Andrew Goffey, _“The Obscure Objects of Object Orientation,”_ in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).
