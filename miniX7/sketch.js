let bg;
let vax = [];
let pipes = [];
let sImg;
let song;
let mode; //determines whether the game has started
let slider;

function preload(){
  bg= loadImage('coronavirus.jpeg');
  sImg = loadImage('needle.png');
  soundFormats('mp3');
  song =loadSound('POKEMON_SONG');
}
function setup(){
  mode = 0;
  slider = createSlider(0,1,0.5,0.01);
  song.play();
  song.loop();
  createCanvas(675,380)
  textSize(18);
  vax = new Vax  ();
  pipes.push(new Pipe());
}

function draw(){
  song.setVolume(slider.value());
  clear();
  if (mode==0){
    background(240,79,84);
    fill(255);
    textFont("Helvetica");
    textAlign(CENTER);
    text('WELCOME, HELP THE VACCINE CATCH THE CORONA VIRUS', width/2,height/2);
    text('press ENTER to start',width/2,height/2+20);
    text('use spacebar to play', width/2, height/2+40);
  }
  if (mode==1){
  background(bg);

  if (frameCount % 75 == 0){
    pipes.push(new Pipe());
    }
  for (let i = pipes.length-1; i >=0; i--){
    pipes[i].show();
    pipes[i].update();

    if (pipes[i].hits(vax)){
      mode=2;
    }
    if (pipes[i].offscreen()){
      pipes.splice(i,1);
    }
}
  vax.update();
  vax.show();

}
if (mode==2){
  pipes=[];
  background(150);
    fill(255);
		textAlign(CENTER);
		text('GAME OVER', width / 2, height / 2);
		text('press SHIFT to play again', width / 2, height / 2 + 20);
  }
}
function keyPressed(){
  if(keyCode===ENTER){
    mode=1;
  }
  if(keyCode===SHIFT){
    mode=0;
  }
  if (key == ' '){
    vax.up();
  }
}
