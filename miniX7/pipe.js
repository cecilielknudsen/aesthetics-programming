
function Pipe(){
  this.spacing =175;
  this.top = random(height/6, 3 /4*height);
  this.bottom = height - (this.top + this.spacing);
  this.x = width;
  this.w = 80;
  this.speed = 4;

  this.highlight = false;

  this.hits = function(vax){
if(vax.x > this.x && vax.x < this.x + this.w){
      if (vax.y < this.top || vax.y > height - this.bottom){
        this.highlight = true;
        return true;
      }
    }
    this.highlight = false;
    return false;
  }

  this.show = function(){
    fill(240,79,84);
    if(this.highlight){
      fill(247,171,7);
    }
    rect(this.x,0,this.w,this.top);
    rect(this.x,height-this.bottom,this.w,this.bottom);
  }

  this.update = function(){
    this.x -= this.speed;
  }
  this.offscreen= function(){
    if(this.x < -this.w){
      return true;
    } else{
      return false;
    }
  }

}
