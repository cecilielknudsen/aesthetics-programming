[Click here to see my work:](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX5/)

**JUST RELAX**
![](class_05_miniX5.png)

**ABOUT MY PROGRAM:**

For this project, I decided to call it "Just Relax", which is named after I made the first piece of code - the dots/points going down the screen. I named the project this after just looking at the way the dots randomly fills up the screen and I found it rather soothing to be honest. That something so somewhat random could be so mesmerizing and relaxing to just sit and stare at. Because the text "blinks"(the outline of the text changes, which makes it look as though it's blinking) and the dots changes color, one might not associate it with being the same code all the way through, which I find facinating because ones eyes just end up following the dots all the way down. Time kind of stands still, when watching.

I wanted to use somewhat muted types of colors, because I find them more peaceful and again soothing in a sense. I wanted it to be a contrast to when looking at work like the 10PRINT program and the Langton's Ant, which I find seems more loud. 10Print's bright blue and Langton's Ant's black/white can seem more "chaotic" and "load", which is why I wanted to do the opposite.  

**What role do rules and processes have in your work?**

My program is build up with a for-loop, which is the text and how the outline keep changing color, and a conditional if-statement, which is the dots/points going from one side to the other and then downwards. The for-loop just keeps repeating, whereas once the if-statements is done running, one has to refresh the page in order for it to restart. I think if there was something I wanted to change, I might want to make the if-statement to a for-loop instead - just to make the dots reload once they have reached the bottom of the page.

**The idea of “auto-generator”**

I found the idea of randomness of the whole operation quite interesting - the fact that everytime one refreshes/restarts the program, the position of the dots are in a completely new place with each new start. However, as I also have discussed with my study-group, I do wonder if there might be a time in which there are no new patterns for the dots to be placed. As I understand the auto-generator through the required reading and doing the miniX, it is not completely random, the way it operates is thorugh probability made through variables. For instance the way the dots changes color is through the random function, which just picks a color between 0 and 255, which makes it grayscale.      

_**References:**_ 

_Video:_[The Coding Train,"Coding Challenge #76:10PRINT in p5.js"](https://www.youtube.com/watch?v=bEyTZ5ZZxZs)

_Website/book:_ [10PRINT CHR$(205.5+RND(1)); : GOTO 10](https://10print.org/)

_Book:_ Soon, Winnie, Cox, Geoff, _"A Handbook of Software Studies"_ , 2020, p.123-141.
