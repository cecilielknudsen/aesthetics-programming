let x=10;
let y=10;
let spacing= 30;
let s = 'just relax :)';

function setup(){
  createCanvas(windowWidth,windowHeight);
  background(139,180,199);
  frameRate(5);

}

function draw(){

  for(var i= 0; i<= width; i= i+50){
    textSize(70);
    textAlign(CENTER);
    fill(199,139,150);
    textFont('monospace',80);
    text(s,windowWidth/2,windowHeight/2);
  }

  stroke(random(255,11,134));
  strokeWeight(15);
  if (random(5)< 2.5){
    point(x,y,x+spacing,y+ spacing);
  } else{
    point(x,y+spacing,x+spacing,y)
  }
  x = x + spacing;
  if (x>width){
    x=0;
    y= y + spacing;
  }

    
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
