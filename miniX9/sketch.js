let article;
let api = 'https://api.nytimes.com/svc/search/v2/articlesearch.json?q=';
let apiKey = '&api-key=H1aJS2J1q4AgxT2AnZgfEW14NrtK9Mp9';
let keywords = 'police+violence&black+lives+matter';
let s = "STOP INJUSTICE";
let birds;

function setup() {
  createCanvas(windowWidth,windowHeight);
  frameRate(0.5);

}

function preload(){
  birds = loadFont('euphorigenic.ttf')
  let url = api + keywords + apiKey;
  loadJSON(url, gotData);
}

function gotData(data){
  //println(data);
  article = data;
  print(data);
}
function draw() {
  background(0);

  textFont(birds);
  textSize(90);
  textAlign(CENTER,CENTER);
  fill('red');
  text(s,750,400);
  //Article headlines
  if (article){
    var value = article.response.docs[0].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value,random(windowWidth),random(windowHeight));

    var value1 = article.response.docs[1].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value1,random(windowWidth),random(windowHeight));


    var value2 = article.response.docs[2].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value2,random(windowWidth),random(windowHeight));


    var value3 = article.response.docs[3].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value,random(windowWidth),random(windowHeight));


    var value4 = article.response.docs[4].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value4,random(windowWidth),random(windowHeight));


    var value5 = article.response.docs[5].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value5,random(windowWidth),random(windowHeight));


    var value6 = article.response.docs[6].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value6,random(windowWidth),random(windowHeight));


    var value7 = article.response.docs[7].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value7,random(windowWidth),random(windowHeight));


    var value8 = article.response.docs[8].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value8,random(windowWidth),random(windowHeight));


    var value9 = article.response.docs[9].headline.main;
    noStroke();
    textFont(birds);
    textSize(30);
    fill(255);
    text(value9,random(windowWidth),random(windowHeight));

  }
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
