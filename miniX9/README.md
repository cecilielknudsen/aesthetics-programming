**STOP INJUSTICE**

By Mie and Cecilie, Group 11 :) 

[See our project here](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX9/)


![](miniX9.png)

**What is the program about?**

The program consists of headlines that are shown when you search for police violence and black lives matter on the New York Times. The purpose of the program is to visualize how big the problems of police violence are. We wanted to put this topic in purpose, because right now the biggest topic in all countries is covid-19, and other very important topics are put in the background because covid has such an impact on all of us. We think that it is still important to remember and to justify the people living on the earth, even though there is a massive pandemic above us. 
When entering the program the first thing you see is the red headline “STOP INJUSTICE” that we made to capture your attention to the topic and the purpose of this minix. The red color is obviously chosen to catch the eye of the viewer, and because we wanted a dramatic color for the message. The many headlines of articles about this exact topic, is the next thing that you will see, and these will underline the message that we are trying to send. We chose the background color to be black, and the color of the text to be white, to make it more prominent. 
For this program we used an API from the New York Times for searched articles. Within this we chose to create some keywords to reduce the section and to get a perfect view on the topic that we wanted our program to be about. 

**Syntax**

First we created the canvas to be as big as the window height and width. We also used the function windowResized, to make the interface resize within the canvas when changed, so it will adapt to the size of the window, and the user will be able to see the whole program even though they scaled their window. 
Subsequently we chose the framerate to be 0.5, so the text will move slowly, and the user will be able to read it. In the preload function we loaded our API, the keywords and the API key, to be able to use the data in our program. Furthermore we loaded the text font in the preload function. We also used the function gotData, to run the articles, and to allow the program to print the different headlines on the interface. In the function draw, we used an if condition statement to make the different headlines of the articles appear on screen. As mentioned above, we chose a white color for the text. 

**Description and reflection on our process**	
	
In our process in finding the right subject to choose, we went onto the New York Times’ website and looked at some of its headlines. We noticed that so much violence and injustice was being reported, especially in the United States. School shootings are always a current issue, but the Black Lives Matter movement has really been on the rise these past couple of years. In this global pandemic, in particular, kicking off 2020 with the injustice of the African-american citizens - e.g. the murders of George Floyd and Breonna Taylor. As we looked through the articles, we found it hard to look at. So many lives have been ruined by police brutality and violence. This is one of the reasons why we wanted to highlight police violence and the Black Lives Matter movement, as it is still a critical debate in today's world and society. 


We wanted to make the many different headlines about these issues be presented on the canvas. The main focus of this specific API was to highlight these just as important issues, where we call forward the data on the website and gather it (in this case headlines) in one place. Through the API key and keywords in the URL, we are able to sort the data and only give us the requested data. For instance, our requested data is in the keywords: Police+violence and black+lives+matters. The power relations of this API give us (the viewer) an opportunity to process all the information in one place, without having to go to the website and search for the articles themselves. API holds a great significance in the digital culture. As every IT device nowadays uses an API, like Facebook, Tiktok, and so on, it makes it easier and more accessible to gather information for whatever one’s heart might desire. Like when one turns on one’s computer, APIs are used to boot the operating system, or when one runs a program on said computer, the program will use APIs to run it in the operating system.
Something that made us wonder, was ‘are APIs only used to send data back and forth, or do they have other abilities?’. That is something we would like to look more into.       


**Reflection on further investigating on APIs**


When looking for the API key, we wondered how it is so easy to get access to data. We just had to sign up and then we got the API key for ‘searched articles’ on the New York Times. This gives us an opportunity to create something with information that is already found, so that we, in our case, could spend more time creating the final work. As mentioned above, API makes it more accessible to gather information, which we also found out when working on this minix. So, a question for further investigating on APIs would for us be; Does APIs have other abilities than sending and saving this information? We would like to investigate further in this to create a bigger understanding of APIs, because so far, we have a very positive look at the API system. Even though it is easy to get the necessary information when searching for something, and that it can monitor who is getting the data and what it is used for, you need to have an API key for access. Because of that, we think that APIs are very safe even though they sometimes can retain private information or data about something/ someone. 
				
			
		
