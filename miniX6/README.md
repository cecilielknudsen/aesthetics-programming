**Revisiting the past** 

MiniX6
[Click here to see my project:](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX6/)

![](class06_miniX6.png)

**•	Which MiniX do you plan to rework?**

![](miniX1_class.01.png)
I decided to work on my MiniX1, as it was my very first time ever trying programming and I felt it had the most to improve on.I really wanted to implement the syntax I have learn recently – like the random-function and the for-loop. 
It was quite interesting to see how much I have learn already, because I clearly knew what I wanted to change, and how I could do so.

**•	What have you changed and why?**

The canvas size so it looks more streamline and polished – also added the windowResized function so the canvas resizes when the window screen does. I also wanted there to be more movement, so by making a for loop I made the ellipses appear on the canvas randomly. And put on the framerate so I could control how fast they’re going. I made the ellipses different colors – I got a kind of psychedelic vibe of the piece, because of the colors. The yellow, green, and blue ellipses contrast really well with the pink background.    


**•	How would you demonstrate aesthetic programming in your work?**

I tried to make my piece more aesthetically pleasing to look at for one. The ellipses, in the first miniX, was manually put in and I remember I couldn’t quite get them where I wanted to, because I hadn’t figured out the way the canvas worked yet. Also I feel like the whole piece look more pleasing to the eye without the border (stroke) around the ellipses. 


**•	What does it mean by programming as a practice, or even as a method for design?**

Programming is time consuming when using it as a tool or method. There’s a lot of planning involved. It’s not just about using the programming language (the different syntaxes and concepts), but rather what one does and makes with that knowledge, e.g. art. Furthermore, any programmer might know what a for-loop, although I knew that I wanted the ellipses to do something specific in my piece and thus used the for-loop to achieve the wanted outcome. With this argument, programming becomes more than just lines of code. It becomes a tool or a way of thinking to come up with ideas for new programs or even designs.


**•	What is the relation between programming and digital culture?**

It’s inevitable that the digital culture becomes more and more apart of everyday-life and thereby also (aesthetic) programming has, more and more, a strong hold on the digital art scene. Programming makes experiences more mobile – you can take it everywhere with you on almost any digital device. I dare even argue that programming is the gateway to digital culture – without programming, we are not even able to be on this platform, reading these words.  


**•	Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate the perspective of critical aesthetics?**

I think when it comes to both Aesthetic Programming and Critical Making, is that both is very reflective practices. Like Hertz mentions in the text a list of methods that can be used in the process of reflective design (Ratto et al. p. 25) screenshot her.

![](hertz_citat.png)

It is of course important when making something (anything really) of value, that one actually really think about what the message is they are trying to convey. Like programming as I mentioned above, both these concepts can be used as tools or ways of thinking, when making something. However, I think for this particular project, I was not trying to communicate anything meaningful. I wanted it to be lighthearted, like my first miniX, where it’s just trying out something fun and new and just see where it goes. However, that might also send some sort of message, regardless if I want it to or not. Who knows?   


**REFERENCES:**

**Text:** Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28.

**Book:** Aesthetic Programming. "A handbook of Software Studies" Soon, Winnie. Cox, Geoff.

I took some inspiration from this sketch:
**Website:** https://editor.p5js.org/kellylougheed/sketches/K6PoaHCNv
