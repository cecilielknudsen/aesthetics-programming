function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  frameRate(2);
}
function draw() {
  // put drawing code here
  noFill();
  colorMode(RGB,255,30,250,1);
  background('#fae');
  strokeWeight(4);
  noStroke();

  if(mouseIsPressed){
    fill('rgba(100%,0%,100%,0.5)');
  } else {
    fill('rgba(254,72,187,1)');
  }
  if(mouseY<200) {
    ellipse(mouseX,mouseY,75,75)
    }
    else{
      ellipse(mouseX,mouseY,250,250)
    }

    noStroke();
    for(let i =0; i< 100; i++){
      fill(random(254,13,155,1), random(254,72,187,1), random(254,72,221,1));
      let size = random(90);
      ellipse(random(windowWidth)+50, random(windowHeight)+50, size,size)
}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}
}
