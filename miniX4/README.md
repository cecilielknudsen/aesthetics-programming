[See my project here!](https://cecilielknudsen.gitlab.io/aesthetics-programming/miniX4/)

**"PROTECTING DATA?"**

![](miniX4_class.png)

In a world full of digitalization - surveillance and data capture is at its peak. When wanting to stay anonymous on public footage, a rectangle is often put in front of your eyes and suddenly no one can identify you. However one is still able to see almost everything (other than your eyes) that can identify you - gender, age, height, build etc. Something that's supposed to protect from being identified, is not so protecting at all? 

My program consists of a web cam, where one's face is being tracked and a rectangle is following the eyes. The syntaxes used for this assignment was mainly a buttom function, the cml-tracker and capture, which is the ones tracking your face and some "if" statements and "for" statements. I found this assignment very complex and rather difficult, so most of the time spend on this, was trying to get it to work. I learnt how to change the filter of the imagery and how to make the "censored" box move with the face. All in all, everything I did in this assignment was new to me and thus also quite challenging.

My program also concerns how people are not quite safe and anomynous, even though one might think they are. One can still be identified and therefore data captured. We might not even realise it and thus it can be quite dangerous. The cultural implications of data capture is more and more evident. As more and more people create a life for themselves online - via Facebook, Instagram, Youtube etc. - the more companies use the data people so willingly put on these platforms. Again, they might not even realize they are doing it. 
As **Shoshana Zuboff** states it in the VPRO Ducumentary "_Survaillance capitalism_"(2019):

> "The term survaillance capitalism is not an arbitrary term. Why? Survaillance. Because it must be operations that are engineered as undetectable, indecipherable, cloaked in rhetoric that aims to misdirect, obfuscate, and just downright bamboozle all of us, all the time"

According to Zuboff, the data we give to websites like Facebook and/or Youtube, get sold off to "the highest bidder", meaning that companies buy your data in order to sell you adverts etc. of their products. I think most people (like myself) only know the tip of the iceberg, where they think they're still in control of their actions, however as Zuboff puts it in the documentary, we haven't in a long time. As it isn't as known to people yet, I think many are like "_Well, I haven't got nothing to hide anyway, what does it matter if a accept these cookies (i.e)?_".

However, I believe the more people get informed and edducated on the subject, the more transparency we get from the companies and thereby the more control users will get.
